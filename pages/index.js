// import styles from '../styles/Home.module.css'
import Navbar from './components/Navbar'
import Topban from './components/Topban';


function Home() {
  return (
    <div>
      <Navbar/>
      <Topban/>
    </div>
  )
}
export default Home;

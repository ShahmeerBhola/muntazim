import Link from "next/link";
import React,{useState} from "react";
import style from "../components/Navbar.module.css";
import MenuOutlinedIcon from "@material-ui/icons/MenuOutlined";

function Navbar() {
    const [isCollapse,setCollapse]=useState(true);
    const  handleCollapse=()=>{
        setCollapse(!isCollapse);
    }
  return (
    <div className={style.container}>
      <div className={style.navbar}>
        <div className={style.logo}>
          <img src="/muntazim-logo.png" className={style.logo1} alt="logo" />
        </div>
        <div className={style.link}>
          <Link href="#">
            <a className={style.link1}>Home</a>
          </Link>
          <Link href="#">
            <a className={style.link1}>Features</a>
          </Link>
          <Link href="#">
            <a className={style.link1}>How Its Work</a>
          </Link>
          <Link href="#">
            <a className={style.link1}>About Us</a>
          </Link>
          <button className={style.btn}>
            <a className={style.btntext}>Download</a>
          </button>
          <div className={style.displayflex}>
            <button className={style.hamburger} onClick={handleCollapse}>
              <MenuOutlinedIcon className={style.icon} />
            </button>
          </div>
        </div>
      </div>
      <ul className={isCollapse?style.toggler:style.toggler1} style={{padding:"5px",margin:"5px"}}>
          <li className={style.collapse}><a className={style.text1}>Home</a></li>
          <li className={style.collapse}><a className={style.text1}>Features</a></li>
          <li className={style.collapse}><a className={style.text1}>How It's Work</a></li>
          <li className={style.collapse}><a className={style.text1}>About Us</a></li>
      </ul>
    </div>
  );
}

export default Navbar;

import React from 'react';
import styles from '../components/Topban.module.css';
import PhonelinkIcon from '@material-ui/icons/Phonelink';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';

 function Topban() {
  return (
    <div>
        <section className={styles.bannerStatic} >
        <div className={styles.container}>
            <div className={styles.section1}>
                <div className={styles.column1}>
                        <h3>Awesome App That Handle All your Event Preparations</h3>
                        <p>This app can handle all your life events preparations from catering to decoration, photography and venue selection.</p>
                        <p></p>
                        <div>
                            <button href="#" className={styles.btn1}><span>Download App</span></button>
                            <button href="#" className={styles.btn2}><span>Discover More</span></button>
                        </div>
                </div>
                    <div className={styles.column2}>
                        <img src="/01.png" alt=""/>
                    </div>
            </div>
   
        </div>
    </section>
    <section className={styles.bannerwork} >
        <div className={styles.container3}>
            <div className={styles.section3} >
                <div className={styles.sec3_col1}>
                    <img src="/fature-moc.png" alt=""/>
                </div>
                <div className={styles.sec3_col2} >
                    <h3>How does this App Work?</h3>
                    <img src="/gradient-sep.png"/>
                    <div >
                        <div className={styles.sec3_information}>
                            <div>
                                <PhonelinkIcon style={{fontSize:"45px"}}/>
                            </div>
                            <div className={styles.sec3_inform}>
                                <h4>Make a profile</h4>
                                <p>
                                We use a customized application tobe 
                                <br/>
                                specifically designed a testing gnose 
                                <br/>
                                to keep away for people.
                                </p>
                            </div>
                        </div>
                        <div className={styles.sec3_information}>
                            <div>
                                <PhonelinkIcon style={{fontSize:"45px"}}/>
                            </div>
                            <div  className={styles.sec3_inform}>
                                <h4>Download it for Free</h4>
                                <p>
                                We use a customized application tobe 
                                <br/>
                                specifically designed a testing gnose 
                                <br/>
                                to keep away for people.
                                </p>
                            </div>
                        </div> 
                        <div  className={styles.sec3_information}>
                            <div>
                                <PhonelinkIcon style={{fontSize:"45px"}}/>
                            </div>
                            <div className={styles.sec3_inform}>
                                <h4>Enjoy this app</h4>
                                <p>
                                We use a customized application tobe 
                                <br/>
                                specifically designed a testing gnose 
                                <br/>
                                to keep away for people.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div> 
        </div>
    </section>
    <section className={styles.bannerevent} >
        <div className={styles.container4}>
            <div className={styles.section4} >
                <div className={styles.sec4_col2} >
                    <VerifiedUserIcon style={{fontSize:"45px"}}/>
                    <h3>Easy to Manage Your All Events by This App</h3>
                    <p>If you want to manage all your events without any hastle then just downloaded the app and explore the amazing app feature that help you in managing your events.</p>
                        <div className={styles.sec4_arrow}>
                        <a href="#">Discover</a>
                        <ChevronRightIcon/>
                        </div>
                </div>
                <div className={styles.sec4_col1}>
                    <img src="/Purpose.png" alt=""/>
                </div>
            </div> 
        </div>
    </section>
    <section className={styles.bannerblog} >
        <div className={styles.container5}>
            <div className={styles.section5} >
                <div className={styles.sec5_col1}>
                    <img src="/mockup.png" alt=""/>
                </div>
                <div className={styles.sec5_col2} >
                    <PhonelinkIcon style={{fontSize:"45px"}}/>
                    <h3>We Are Bridging A gap Between Customers And Vendors</h3>
                    <p>In order to schedule or manage an event Just download the app and explore the features as per your desires. This app surely make you amaze by it's top notch features</p>
                    <h3 style={{color:"#e53767"}}>You gonna love it! </h3>
                        <div className={styles.sec5_arrow}>
                        <a href="#">Discover</a>
                        <ChevronRightIcon/>
                        </div>
                </div>
            </div> 
        </div>
    </section>
    <section className={styles.section6}>
        <div className={styles.container6}>
            <h3>Watch a Quick Tutorial</h3>
            <a><PlayCircleFilledIcon className={styles.play}/></a>
        </div>
    </section>
    <section className={styles.bannerblog} >
        <div className={styles.container5}>
            <div className={styles.section5} >
                <div className={styles.sec5_col1}>
                    <img src="/mockup.png" alt=""/>
                </div>
                <div className={styles.sec5_col2} >
                    <PhonelinkIcon style={{fontSize:"45px"}}/>
                    <h3>We Are Bridging A gap Between Customers And Vendors</h3>
                    <p>In order to schedule or manage an event Just download the app and explore the features as per your desires. This app surely make you amaze by it's top notch features</p>
                    <h3 style={{color:"#e53767"}}>You gonna love it! </h3>
                        <div className={styles.sec5_arrow}>
                        <a href="#">Discover</a>
                        <ChevronRightIcon/>
                        </div>
                </div>
            </div> 
        </div>
    </section>

    
    </div>
  );
}
export default Topban;
